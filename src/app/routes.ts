import { ListComponent } from "./todo/components/list/list.component";
export let routes = [
  { path: '', component: ListComponent, pathMatch: 'full' },
  { path: ':status', component: ListComponent }
];