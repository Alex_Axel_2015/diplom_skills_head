import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './components/list/list.component';
import { FooterComponent } from './components/footer/footer.component';
import { ItemComponent } from './components/item/item.component';
import { HeaderComponent } from './components/header/header.component';
import { TrimPipe } from './pipes/trim.pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    ListComponent, 
    FooterComponent, 
    ItemComponent, 
    HeaderComponent, 
    TrimPipe
  ],
  exports: [
    ListComponent, 
    FooterComponent,
    ItemComponent, 
    HeaderComponent
  ]
})
export class TodoModule { }
