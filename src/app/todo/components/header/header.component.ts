import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';

@Component({
  selector: 'todo-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  newTodo = '';
  _todoStore;

  constructor(todoStore:StoreService) {
		this._todoStore = todoStore;
	}

	addTodo() {
		if (this.newTodo.trim().length) {
			this._todoStore.add(this.newTodo);
			this.newTodo = '';
		}
	}

  ngOnInit() {
  }

}
