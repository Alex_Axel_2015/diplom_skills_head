import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'todo-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  _todoStore;
  _route;
  currentStatus;

  constructor(todoStore: StoreService, route: ActivatedRoute) {
    this._todoStore = todoStore;
    this._route = route;
    this.currentStatus = '';
  }

  ngOnInit() {
    
  }

  removeCompleted() {
    this._todoStore.removeCompleted();
  }

  getCount() {
    return this._todoStore.todos.length;
  }

  getRemainingCount() {
    return this._todoStore.getRemaining().length;
  }

  hasCompleted() {
    return this._todoStore.getCompleted().length > 0;
  }

}
