import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'todo-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  _todoStore;
  _route;
  _currentStatus;

  constructor(todoStore: StoreService, route: ActivatedRoute) {
		this._todoStore = todoStore;
		this._route = route;
		this._currentStatus = '';
	}

	ngOnInit() {
		
	}

	remove(uid) {
		this._todoStore.remove(uid);
	}

	update() {
		this._todoStore.persist();
	}

	getTodos() {
		if (this._currentStatus == 'completed') {
			return this._todoStore.getCompleted();
		} else if (this._currentStatus == 'active') {
			return this._todoStore.getRemaining();
		} else {
			return this._todoStore.todos;
		}
	}

	allCompleted() {
		return this._todoStore.allCompleted();
	}

	setAllTo(toggleAll) {
		this._todoStore.setAllTo(toggleAll.checked);
	}

}
