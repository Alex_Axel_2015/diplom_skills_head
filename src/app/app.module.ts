import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TodoModule } from './todo/todo.module';

import { routes } from './routes';
import { StoreService } from './todo/services/store.service';
import { ListComponent } from './todo/components/list/list.component';
import { FooterComponent } from './todo/components/footer/footer.component';
import { HeaderComponent } from './todo/components/header/header.component';
import { ItemComponent } from './todo/components/item/item.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes, {
      useHash: true
    }),
    TodoModule
  ],
  providers: [StoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
